# InstantAlerter
Collaboration tool - narzędzie do komunikacji w zespole. Jego fundamentalną funkcją jest przekazywanie komunikatów poprzez serwer aplikacji do zalogowanych klientów na laptopach, PC, w sieci służbowej.
# Zespół projektowy
Sebastian Bobrowski, Wojciech Podciborski, Filip Wrzesień, Piotr Baczkowski
# Aktualna funkcjonalność
Przekazywanie komunikatów (socket)
# Następna planowana funckonalność
Projekt i implementacja bazy danych
# Szybki start
source env/bin/activate<br>
python3 test_app.py

<b>odpalenie klient-serwer:</b><br />
1-sze okno terminala: <pre>python3 server.py</pre>
2-gie okno terminala: <pre>python3 client.py</pre>