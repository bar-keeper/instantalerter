# app.py
from datetime import datetime

def get_all_communiques():
    from db_connection import db
    results = db.query(Communique).all()
    return results

def has_communique_expired(communique):
    if communique.expiration_date < datetime.now():
        return True
    return False

def find_expired_communiques():
    expired_communiques = []
    communiques = get_all_communiques()
    for communique in communiques:
        if has_communique_expired(communique):
            expired_communiques.append(communique.id)
    return expired_communiques