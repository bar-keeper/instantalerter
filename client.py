import socket
import pickle
from gi.repository import Notify
clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket.connect((socket.gethostname(), 1238))
Notify.init("Instant Alerter")

while True:
    full_message = b''
    new_message = True
    urgence = ""
    message_array = []
    while True:
        message = clientsocket.recv(1000)
        if new_message:
            message_length = len(message)
            new_message = False

        full_message += message

        if len(full_message) >= message_length:
            message_array = pickle.loads(full_message)
            summary = message_array[0]
            body = message_array[1]
            if message_array[2] == 0:
                urgence = "dialog-information"
            if message_array[2] == 1:
                urgence = "dialog-warn"
            if message_array[2] == 2:
                urgence = "dialog-error"
            notification = Notify.Notification.new(
                summary,
                body,
                urgence
            )

            notification.show()
            new_msg = True
            full_message = b""
