CREATE TABLE IF NOT EXISTS `communiques`(
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `first_name` VARCHAR(255) NULL ,
  `last_name` VARCHAR(255) NULL ,
  `message` TEXT NOT NULL ,
  `creation_date` DATE NOT NULL ,
  `expiration_date` DATE NOT NULL
);
