from dataclasses import dataclass
from typing import Protocol
from typing import List
from typing import Any
from typing import Union


@dataclass
class Communique:
    id: Union[int, None]
    first_name: str
    last_name: str
    message: str
    creation_date: str
    expiration_date: str


class ICommuniquesRepo:
    def get_all(self) -> List[Communique]:
        ...

    def get_by_id(self, communique_id: int) -> Communique:
        ...

    def add(self, communique: Communique) -> None:
        ...

    def update(self, communique: Communique) -> None:
        ...

    def delete(self, communique_id: int) -> None:
        ...

    def delete_all(self) -> None:
        ...


class SqliteCommuniquesRepo(ICommuniquesRepo):
    def __init__(self, db_connection: Any):
        self.db_connection = db_connection

    def _to_domain_model(self, raw_communique: dict):
        return Communique(
            id=raw_communique.get('id'),
            first_name=raw_communique.get('first_name'),
            last_name=raw_communique.get('last_name'),
            message=raw_communique.get('message'),
            creation_date=raw_communique.get('creation_date'),
            expiration_date=raw_communique.get('expiration_date'),
        )

    def get_all(self) -> List[Communique]:
        cur = self.db_connection.cursor()
        cur.execute("SELECT * FROM communiques")

        rows = cur.fetchall()

        return [self._to_domain_model(row) for row in rows]

    def get_by_id(self, communique_id: int) -> Communique:
        cur = self.db_connection.cursor()
        cur.execute("SELECT * FROM communiques WHERE id=?", (communique_id,))

        row = cur.fetchone()

        return self._to_domain_model(row)

    def add(self, communique: Communique) -> None:
        sql = """INSERT INTO communiques (
            first_name, last_name, message, creation_date, expiration_date
            ) VALUES(?, ?, ?, ?, ?)"""

        data = (
            communique.first_name,
            communique.last_name,
            communique.message,
            communique.creation_date,
            communique.expiration_date
        )

        cur = self.db_connection.cursor()
        cur.execute(sql, data)

    def update(self, communique: Communique) -> None:
        sql = """UPDATE communiques
                      SET first_name = ? ,
                          last_name = ? ,
                          message = ?,
                          creation_date = ?,
                          expiration_date = ?,
                      WHERE id = ?"""

        data_to_update = (
            communique.first_name,
            communique.last_name,
            communique.message,
            communique.creation_date,
            communique.expiration_date,
            communique.id,
        )

        cur = self.db_connection.cursor()
        cur.execute(sql, data_to_update)

        self.db_connection.commit()

    def delete(self, communique_id: int) -> None:
        sql = "DELETE FROM communiques WHERE id=?"
        cur = self.db_connection.cursor()
        cur.execute(sql, (communique_id,))
        self.db_connection.commit()

    def delete_all(self) -> None:
        sql = "DELETE FROM communiques"
        cur = self.db_connection.cursor()
        cur.execute(sql)
        self.db_connection.commit()
