import socket
import time
import pickle
from pymongo import MongoClient
client = MongoClient()
db = client.alerts

users = []
notes = []
urgent = []
mongo_data = db['alerts'].find({})

for document in mongo_data:
    index = 0
    for row in document.values():
        index = index +1
        if index == 3:
            users.append(row)
        if index == 4:
            urgent.append(row)
        if index == 1:
            notes.append(row)


serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((socket.gethostname(), 1238))
serversocket.listen(5)
message_number = 0

while True:
    clientsocket, address = serversocket.accept()
    print(address)
    print("connected")

    while True:
        final_object = [users[message_number], notes[message_number], urgent[message_number]]
        message = pickle.dumps(final_object)
        time.sleep(5)
        clientsocket.send(message)
        message_number = message_number + 1

    clientsocket.close()
