# test_app.py
import unittest
import datetime as dt
from mock import Mock, patch
from app import has_communique_expired, find_expired_communiques

class TestApp(unittest.TestCase):

    def setUp(self):
        self.today = dt.datetime.now()
        self.yesterday = dt.datetime.now() - dt.timedelta(days=1)
        self.day_before = dt.datetime.now() - dt.timedelta(days=2)
        self.tomorrow = dt.datetime.now() + dt.timedelta(days=2)

    def test_has_communique_expired(self):
        '''
        The function `has_communique_expired()` takes a argument `communique` which is a 
        SQLAlchemy database object. The object contains attributes `id`, 
        `firstname`, `lastname`, `creation_date` and `expiration_date`.

        If the `expiration_date` is less than the date today it will 
        return `True` or if the `expiration_date` is greater than today 
        then return `False`

        For this test since we do not want to speak with the database we
        will create a `Mock` object and assign it the attributes that 
        a communique has. This `Mock` object is just another python class that when
        called for any fake attribute will return an answer.
        '''

        # create a mock `communique` object
        communique = Mock()

        # now set the attributes for the `communique` object
        communique.id = 1
        communique.firstname = 'John'
        communique.lastname = 'Smith'
        communique.message = 'Example message'
        communique.creation_date = self.day_before
        communique.expiration_date = self.yesterday

        # now we pass the `communique` object check for expected result `True`
        result = has_communique_expired(communique)
        self.assertTrue(result)

        # for the same communique we can update the `expiration_date` to sometime
        # in the future to see if the expected result is `False`
        communique.expiration_date = self.tomorrow
        result = has_communique_expired(communique)
        self.assertFalse(result)

    def test_find_expired_communiques(self):
        """
        We patch the function `get_all_communique()` with our own function
        `mocked_get_all_communiques` for which we set a `return_value`.
        The `return_value` will contain a list of `Mock` communiques. 
        Some of these communiques are expired and some of them haven't expired. 
        We compare the output of our mocked `get_all_communiques()` function with 
        the `expected` output.
        """

        with patch('app.get_all_communiques') as mocked_get_all_communiques:
            mocked_get_all_communiques.return_value = [
                Mock(id=1, firstname='John', lastname='Smith1', message="Example message 1",
                     creation_date=self.today, 
                     expiration_date=self.tomorrow),
                Mock(id=2, firstname='John', lastname='Smith2', message="Example message 2",
                     creation_date=self.yesterday, 
                     expiration_date=self.tomorrow),
                Mock(id=3, firstname='John', lastname='Smith3', message="Example message 3",
                     creation_date=self.day_before, 
                     expiration_date=self.tomorrow),
                Mock(id=4, firstname='John', lastname='Smith4', message="Example message 4",
                     creation_date=self.day_before, 
                     expiration_date=self.yesterday),
                Mock(id=5, firstname='John', lastname='Smith5', message="Example message 5",
                     creation_date=self.day_before, 
                     expiration_date=self.today),
            ]
            results = find_expired_communiques()
            expected = [4, 5] # we know only communique `id` 4 and 5 have expired
            self.assertEqual(results, expected)

if __name__ == '__main__':
    unittest.main()