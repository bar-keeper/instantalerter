import pytest
import datetime

from repositories.sqlite_db import get_db_connection
from repositories.repository import SqliteCommuniquesRepo
from repositories.repository import Communique


@pytest.fixture()
def prepare_db():
    connection = get_db_connection("file::memory:?cache=shared")

    cur = connection.cursor()
    cur.execute("""
    CREATE TABLE IF NOT EXISTS `communiques`(
      `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `first_name` VARCHAR(255) NULL ,
      `last_name` VARCHAR(255) NULL ,
      `message` TEXT NOT NULL ,
      `creation_date` DATE NOT NULL ,
      `expiration_date` DATE NOT NULL
    );
    """)

    yield

    cur.execute("""
    DROP TABLE communiques
    """)


def test_add(prepare_db):
    connection = get_db_connection("file::memory:?cache=shared")
    repo = SqliteCommuniquesRepo(connection)

    data = Communique(
        id=None,
        first_name="Test",
        last_name="Test",
        message="Test msg",
        creation_date=str(datetime.datetime.now()),
        expiration_date=str(datetime.datetime.now()),
    )

    repo.add(data)

    communiques = repo.get_all()

    assert len(communiques) == 1
    assert communiques[0].first_name == data.first_name
    assert communiques[0].last_name == data.last_name


def test_get_by_id(prepare_db):
    connection = get_db_connection("file::memory:?cache=shared")
    repo = SqliteCommuniquesRepo(connection)

    data = Communique(
        id=None,
        first_name="Test",
        last_name="Test",
        message="Test msg",
        creation_date=str(datetime.datetime.now()),
        expiration_date=str(datetime.datetime.now()),
    )

    repo.add(data)

    communique = repo.get_by_id(1)

    assert communique.first_name == "Test"
    assert communique.last_name == "Test"


